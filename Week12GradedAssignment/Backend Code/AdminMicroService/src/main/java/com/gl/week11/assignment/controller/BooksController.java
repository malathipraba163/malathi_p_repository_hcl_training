package com.gl.week11.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.week11.assignment.bean.Books;
import com.gl.week11.assignment.service.BooksService;

@RestController
@RequestMapping("/books")
public class BooksController {
	@Autowired
	BooksService booksService;
	
	@GetMapping(value = "getAllProduct",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Books> getAllProductInfo() {
		return booksService.getAllBooks();
	}
	@PostMapping(value = "storeProduct",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBooksInfo(@RequestBody Books book) {
		
				return booksService.storeBooksInfo(book);
	}
	
	@DeleteMapping(value = "deleteBooks/{id}")
	public String storeProductInfo(@PathVariable("id") int id) {
					return booksService.deleteBooksInfo(id);
	}
	
	@PatchMapping(value = "updateBooks")
	public String updateBooksInfo(@RequestBody Books book) {
					return booksService.updateBooksInfo(book);
	}
	

}
