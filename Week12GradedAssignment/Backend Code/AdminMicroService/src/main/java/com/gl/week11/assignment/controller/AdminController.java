package com.gl.week11.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.week11.assignment.bean.Admin;
import com.gl.week11.assignment.service.AdminService;


@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	AdminService adminService;
	
	@PostMapping(value = "signUp",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String signUp(@RequestBody Admin admin) {
		return adminService.signUp(admin);
	}
    @PostMapping(value="signIn",consumes=MediaType.APPLICATION_JSON_VALUE)
    public String signIn(@RequestBody Admin admin) {
		return adminService.signIn(admin);
	}
    @GetMapping(value="logout")
    public String logout() {
    	return "Logout Successfully";
    }
    
}
