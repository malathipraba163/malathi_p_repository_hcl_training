package com.gl.week11.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.week11.assignment.bean.User;
import com.gl.week11.assignment.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	UserService userService;
	@GetMapping(value = "getAllUser",
			produces = MediaType.APPLICATION_JSON_VALUE)
			public List<User> getAllProductInfo() {
				return userService.getAllUsers();
			}
			@PostMapping(value = "storeUser",
					consumes = MediaType.APPLICATION_JSON_VALUE)
			public String storeProductInfo(@RequestBody User user) {
				
						return userService.storeUserInfo(user);
			}
			
			@DeleteMapping(value = "deleteUser/{id}")
			public String storeProductInfo(@PathVariable("id") int id) {
							return userService.deleteUserInfo(id);
			}
			
			@PatchMapping(value = "updateUser")
			public String updateProductInfo(@RequestBody User user) {
							return userService.updateUserInfo(user);
			}
	

}
