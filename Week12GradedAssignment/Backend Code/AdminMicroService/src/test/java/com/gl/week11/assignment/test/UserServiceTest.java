package com.gl.week11.assignment.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.gl.week11.assignment.bean.User;
import com.gl.week11.assignment.dao.UserDao;
import com.gl.week11.assignment.service.UserService;
@SpringBootTest
class UserServiceTest {
	@InjectMocks
	UserService userService;
	
	@Mock
	UserDao userDao;
	
	@Test
	void testGetAllUsers() {
		//fail("Not yet implemented");
		List<User> list = new ArrayList<User>();
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		list.add(user1);
		list.add(user2);
		list.add(user3);
		when(userDao.findAll()).thenReturn(list);
		List<User> userList = userService.getAllUsers();
		assertEquals(3, userList.size());
		verify(userDao, times(1)).findAll();
	}

	@Test
	void testStoreUserInfo() {
		//fail("Not yet implemented");
		User user = new User();
		userService.storeUserInfo(user);
		verify(userDao, times(1)).save(user);
	}

	@Disabled
	@Test
	void testDeleteUserInfo() {
		fail("Not yet implemented");
	}

	@Disabled
	@Test
	void testUpdateUserInfo() {
		fail("Not yet implemented");
	}

}
