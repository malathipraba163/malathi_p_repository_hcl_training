package com.gl.week11.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gl.week11.assignment.bean.User;
@Repository
public interface UserDao  extends JpaRepository<User, String>{
	public User findByUsernameAndPassword(@Param("username") String username,@Param("password") String password);

	public static Object getUserFromDb() {
		// TODO Auto-generated method stub
		return null;
	}

}
