package com.gl.week2.GradedAssignemnt;
import java.util.*;
import java.util.Collections;

public class EmpDataStructureB {
	
	public void cityNameCount(ArrayList<Employee> employees) {
		 Set<String> l = new TreeSet<String>();
	     ArrayList<String> a = new ArrayList<String>();
	     System.out.println("Count of employees from each city:\n");
	     System.out.print("{");
	     for (Employee s : employees) {
	          a.add(s.getCity());
	          l.add(s.getCity());
	       }
	      for (String s : l) {
	         System.out.print(s + "=" + Collections.frequency(a, s) + " ");
	   }
	     System.out.print("}");
	}
	public void monthlySalary(ArrayList<Employee>employees) 
	{
		System.out.println("Monthly Salary of employee along with their ID is:\n");
	     System.out.print("{");
	     for (Employee e : employees) {
	           System.out.print(e.getId() + "=" + (float)e.getSalary() / 12 + " ");
	     }
	     System.out.print("}");
	     

	}

}
