package com.gl.week2.GradedAssignemnt;
import java.util.*;

public class Main {

	public static void main(String[] args) throws IllegalArgumentExceptionDemo  {
		
		ArrayList<Employee> l1= new ArrayList<>();
		System.out.println("List Of Employees:\n");
		  Employee obj1 = new Employee();
		    obj1.setId(1);
			obj1.setName("Aman");
			obj1.setAge(20);
			obj1.setSalary(1100000);
			obj1.setDepartment("IT");
			obj1.setCity("Delhi");
			l1.add(obj1);
			
			
			Employee obj2 = new Employee();
			obj2.setId(2);
			obj2.setName("Bobby");
			obj2.setAge(22);
			obj2.setSalary(500000);
			obj2.setDepartment("HR");
			obj2.setCity("Bombay");
			l1.add(obj2);
		
			
			Employee obj3 = new Employee();
			obj3.setId(3);
			obj3.setName("Zoe");
			obj3.setAge(20);
			obj3.setSalary(750000);
			obj3.setDepartment("Admin");
			obj3.setCity("Delhi");
			l1.add(obj3);
		
			Employee obj4 = new Employee();
			obj4.setId(4);
			obj4.setName("Smitha");
			obj4.setAge(21);
			obj4.setSalary(1000000);
			obj4.setDepartment("IT");
			obj4.setCity("Chennai");
			l1.add(obj4);
			
			
			Employee obj5 = new Employee();
			obj5.setId(5);
			obj5.setName("Smitha");
			obj5.setAge(24);
			obj5.setSalary(1200000);
			obj5.setDepartment("HR");
			obj5.setCity("Bengaluru");
			l1.add(obj5);
			
			obj1.Display();
			obj2.Display();
			obj3.Display();
			obj4.Display();
			obj5.Display();
       
        System.out.println(" ");
        EmpDataStructureB b = new EmpDataStructureB();
        b.monthlySalary(l1);
        System.out.println("\n");
        b.cityNameCount(l1);
        System.out.println("\n");
        EmpDataStructureA a = new EmpDataStructureA();
        System.out.println("Names of all employees in the sorted order are:\n");
        a.sortingNames(l1);
        System.out.println("\n");
        
        
       
}
		
		
		
	}


