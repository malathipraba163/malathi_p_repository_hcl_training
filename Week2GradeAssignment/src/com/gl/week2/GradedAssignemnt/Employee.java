package com.gl.week2.GradedAssignemnt;


public class Employee {
	 int id;
     String name;
     int age;
     int salary;
     String department;
     String city;
     public void setId(int id) throws IllegalArgumentExceptionDemo {
 		if(id<0){
 	           throw new IllegalArgumentExceptionDemo("Id cannot less than zero");
 	         }
 		else
 		    this.id = id;
 	}
 	public int getId() {
 		return id;
 	}
 	public void setName(String name) throws IllegalArgumentExceptionDemo {
 		if(name==null) 
        	   throw new IllegalArgumentExceptionDemo("Name cannot be null");
 		else
 		    this.name = name;
 	}
 	public String getName() {
 		return name;
 	}
 	public void setAge(int age) throws IllegalArgumentExceptionDemo {
 		 if(age<0)
 	           throw new IllegalArgumentExceptionDemo("Age cannot be zero");
 		 else
 		     this.age=age;
 	}
 	public int getAge() {
 		return age;
 	}
 	public void setSalary(int salary) throws IllegalArgumentExceptionDemo {
 		if(salary<0)
             throw new IllegalArgumentExceptionDemo("Salary cannot be zero");
 		else
 	        this.salary = salary;
 	}
 	public int getSalary() {
 		return salary;
 	}
 	public void setDepartment(String department) throws IllegalArgumentExceptionDemo{
 		if( department == null)
             throw new IllegalArgumentExceptionDemo("Department cannot be null");
 		else
 		    this.department = department;
 	}
 	public String getDepartment() {
 		return department;
 	}
 	public void setCity(String city) throws IllegalArgumentExceptionDemo{
 		if(city==null)
             throw new IllegalArgumentExceptionDemo("City cannot be null");
 		else 
 		    this.city = city;
 	}
 	public String getCity() {
 		return city;
 	}
 	public void Display() {
 		System.out.println( id +" " + name +" "+ age +" "+salary +" "+department +" "+city);

}
}
