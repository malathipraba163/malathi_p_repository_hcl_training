const studentProfile = async () => {
    const response = await fetch("http://localhost:3000/profile")
    const data = await response.json();

    var previous = document.querySelector('.row');
    previous.remove();
    const addrow = document.querySelector('.container-fluid');
    addrow.innerHTML = `<div class="row"></div`;
    for (let i = 0; i < data.length; i++) {

        const movietable = document.querySelector('.row');
        movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">

    <div class="card">
        <ul class="list-group list-group-flush">
        <li class="list-group-item">Student Student id : ${data[i].id}</li>
            <li class="list-group-item">Student Name : ${data[i].Name}</li>
            <li class="list-group-item">Phone : ${data[i].Phone}</li>
            <li class="list-group-item">Subject :${data[i].Subject}</li>
    
        </ul>
       
    </div>
    `
    }}


    
const timeTable = async () => {
    const response = await fetch("http://localhost:3000/TimeTable")
    const data = await response.json();

    var previous = document.querySelector('.row');
    previous.remove();
    const addrow = document.querySelector('.container-fluid');
    addrow.innerHTML = `<div class="row"></div`;
    for (let i = 0; i < data.length; i++) {

        const movietable = document.querySelector('.row');
        movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">

    <div class="card">
        <ul class="list-group list-group-flush">
        <li class="list-group-item">Student Student id : ${data[i].id}</li>
            <li class="list-group-item"> Period : ${data[i].Period}</li>
            <li class="list-group-item">Day: ${data[i].Day}</li>
            <li class="list-group-item">Subject :${data[i].Subject}</li>
    
        </ul>
       
    </div>
    `
    }}




    const monthlyActivity = async () => {
        const response = await fetch("http://localhost:3000/Activities")
        const data = await response.json();

        var previous = document.querySelector('.row');
        previous.remove();
        const addrow = document.querySelector('.container-fluid');
        addrow.innerHTML = `<div class="row"></div`;
        for (let i = 0; i < data.length; i++) {

            const movietable = document.querySelector('.row');
            movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">

        <div class="card">
            <ul class="list-group list-group-flush">
            <li class="list-group-item">Student Student id : ${data[i].id}</li>
                <li class="list-group-item"> Activity : ${data[i].activity}</li>
                <li class="list-group-item">Subject: ${data[i].subject}</li>
        
            </ul>
           
        </div>
        `
        }}


        const extraActivity = async () => {
            const response = await fetch("http://localhost:3000/extraactivity")
            const data = await response.json();
    
            var previous = document.querySelector('.row');
            previous.remove();
            const addrow = document.querySelector('.container-fluid');
            addrow.innerHTML = `<div class="row"></div`;
            for (let i = 0; i < data.length; i++) {
    
                const movietable = document.querySelector('.row');
                movietable.innerHTML = movietable.innerHTML + `<div class="col-md-2 m-a-2">
    
            <div class="card">
                <ul class="list-group list-group-flush">
                <li class="list-group-item">Student Student id : ${data[i].id}</li>
                <li class="list-group-item"> Date : ${data[i].date}</li>

                <li class="list-group-item"> Activity : ${data[i].activity}</li>
               <li class="list-group-item">Subject: ${data[i].subject}</li>
            
                </ul>
               
            </div>
            `
            }}
