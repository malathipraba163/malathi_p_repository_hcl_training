package com.gl.week8.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.week8.bean.Books;
import com.gl.week8.bean.User;
import com.gl.week8.dao.BooksDao;

@Service
public class BooksService {
	@Autowired
	BooksDao booksDao;
	
	public List<Books> getAllBook() {
		return booksDao.getAllBook();
	}
    
	public List<User>getAllUser(){
		return booksDao.getAllUser();
		
	}
}
