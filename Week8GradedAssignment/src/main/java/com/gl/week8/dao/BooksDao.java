package com.gl.week8.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


import com.gl.week8.bean.Books;
import com.gl.week8.bean.User;

@Repository
public class BooksDao {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Books> getAllBook() {
		return jdbcTemplate.query("select * from books", new BooksRowMapper());
	}
	
	
	public List<Books> findBookById(int pid) {
	
		return jdbcTemplate.query("select * from books where id=?",new Object[] {1},new BooksRowMapper());
	}
	public List<User>getAllUser(){
		return jdbcTemplate.query("select * from user where username like ? and password like ?", new Object[] {1},new  UserRowMapper());
		
	}
}
class BooksRowMapper implements RowMapper<Books>{
	@Override
	public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Books p = new Books();
		p.setId(rs.getInt(1));
		p.setName(rs.getString(2));
		p.setGenere(rs.getString(3));
		p.setUrl(rs.getString(4));
		return p;
	}
	

}
class UserRowMapper implements RowMapper<User>{

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		User u=new User();
		u.setUsername(rs.getString(1));
		u.setPassword(rs.getString(2));
		return u;
		
		
	}
	
}
