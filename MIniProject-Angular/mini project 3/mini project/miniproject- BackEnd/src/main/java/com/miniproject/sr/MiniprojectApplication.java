package com.miniproject.sr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.miniproject.sr.pojo.AdminLogin;
import com.miniproject.sr.pojo.Items;
import com.miniproject.sr.pojo.LoginUser;
import com.miniproject.sr.repository.AdminLogRepository;
import com.miniproject.sr.repository.ItemRepository;
import com.miniproject.sr.repository.UserRepository;




@SpringBootApplication
public class MiniprojectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(MiniprojectApplication.class, args);
	}
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ItemRepository bookRepository;
	
	@Autowired
	private AdminLogRepository adminLogRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void run() {
		
	}
	@Override
	public void run(String... args) throws Exception {
		String sql="set FOREIGN_KEY_CHECKS=0";
		int result=jdbcTemplate.update(sql);
	}
	
	@Bean
	public void inserData(){
		LoginUser login1=new LoginUser("Malathi","RS","malathi@gmail.com","908765412","malathi","123");
		userRepository.save(login1);
		
		LoginUser login2=new LoginUser("sandy","RS","sandy@gmail.com","100000000","sandy","12345");
		userRepository.save(login2);
		
		LoginUser login3=new LoginUser("Gokul","Y","gokul@gmail.com","888888888","gok","goal");
		userRepository.save(login3);
		
		LoginUser login4=new LoginUser("Vidhulya","T","vidhu@test.com","9980765432","vidhu","asdf");
		userRepository.save(login4);
		
		AdminLogin admin1=new AdminLogin("jagan", "rr", "jagan@gmail.com", "9999911111", "jack", "900");
		adminLogRepository.save(admin1);

		Items book=new Items("pizza","80","3","IMG001");
		bookRepository.save(book);
		
		Items book1=new Items("sandwich","100","1","IMG002");
		bookRepository.save(book1);
		
		Items book2=new Items("bhelpuri","20","1","IMG003");
		bookRepository.save(book2);
		
		Items book3=new Items("idly sambra ","250","3","IMG004");
		bookRepository.save(book3);
		
		Items book4=new Items("noodles","90","1","IMG005");
		bookRepository.save(book4);
		
		
	}


}
