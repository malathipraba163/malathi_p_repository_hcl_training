package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com")		// Enable @RestController, @Service, @Repository 
@EntityScan(basePackages = "com.gl.week9.bean")							// Enable entity class 
@EnableJpaRepositories(basePackages = "com.gl.week9.dao")
public class BookSpringDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookSpringDemoApplication.class, args);
		System.err.println("Server running on port number 9090");
	}

}
