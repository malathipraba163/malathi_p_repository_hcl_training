package com.gl.week9.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gl.week9.bean.Admin;
import com.gl.week9.bean.Books;
import com.gl.week9.dao.BooksDao;
import com.gl.week9.service.AdminService;
import com.gl.week9.service.UserService;

@Controller
@RequestMapping("/admin")
public class AdminLoginController {
	@Autowired
	private AdminService adminService;

	

	@GetMapping(value = "getAllAdmin",
			produces = MediaType.APPLICATION_JSON_VALUE)
			public List<Admin> getAllAdminInfo() {
				return adminService.getAllAdmin();
			}
			@PostMapping(value = "storeAdmin",
					consumes = MediaType.APPLICATION_JSON_VALUE)
			public String storeAdminInfo(@RequestBody Admin admin) {
				
						return adminService.storeAdminInfo(admin);
			}
			
			@DeleteMapping(value = "deleteAdmin/{id}")
			public String storeAdminInfo(@PathVariable("id") int id) {
							return adminService.deleteAdminInfo(id);
			}
			
			@PatchMapping(value = "updateAdmin")
			public String updateAdminInfo(@RequestBody Admin admin) {
							return adminService.updateAdminInfo(admin);
			}
			

}
