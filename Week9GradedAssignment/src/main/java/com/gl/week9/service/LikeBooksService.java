package com.gl.week9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.week9.bean.Books;
import com.gl.week9.bean.LikeBooks;
import com.gl.week9.dao.BooksDao;
import com.gl.week9.dao.LikeBooksDao;

@Service
public class LikeBooksService {
@Autowired
	
	LikeBooksDao likebooksDao;
	
	public List<LikeBooks> getAlllikebooks() {
		return likebooksDao.findAll();
	}
	public String storelikebooksInfo(LikeBooks book) {
		
		if(likebooksDao.existsById(book.getId())) {
					return "LikeBooks id must be unique";
		}else {
					likebooksDao.save(book);
					return "LikeBooks stored successfully";
		}
}
	public String deletelikebooksInfo(int id) {
		if(!likebooksDao.existsById(id)) {
			return "likeBooks details not present";
			}else {
			likebooksDao.deleteById(id);
			return "likeBooks deleted successfully";
			}	
	}
	public String updatelikebooksInfo(LikeBooks book) {
		if(!likebooksDao.existsById(book.getId())) {
			return "likeBooks details not present";
			}else {
			LikeBooks p	= likebooksDao.getById(book.getId());	// if books not present it will give exception 
			p.setName(book.getName());						// existing product price change 
			likebooksDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "likeBooks updated successfully";
			}	
	}
	

}
