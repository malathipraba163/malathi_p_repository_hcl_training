package com.gl.week9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.week9.bean.Books;
import com.gl.week9.dao.BooksDao;

@Service
public class BooksService {
	
	@Autowired
	
	BooksDao booksDao;
	
	public List<Books> getAllBooks() {
		return booksDao.findAll();
	}
	public String storeBooksInfo(Books book) {
		
		if(booksDao.existsById(book.getId())) {
					return "Books id must be unique";
		}else {
					booksDao.save(book);
					return "Books stored successfully";
		}
}
	public String deleteBooksInfo(int id) {
		if(!booksDao.existsById(id)) {
			return "Books details not present";
			}else {
			booksDao.deleteById(id);
			return "Books deleted successfully";
			}	
	}
	public String updateBooksInfo(Books book) {
		if(!booksDao.existsById(book.getId())) {
			return "Books details not present";
			}else {
			Books p	= booksDao.getById(book.getId());	// if books not present it will give exception 
			p.setName(book.getName());						// existing product price change 
			booksDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "Books updated successfully";
			}	
	}
	

}
