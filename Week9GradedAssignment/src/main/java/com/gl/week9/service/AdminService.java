package com.gl.week9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.week9.bean.Admin;
import com.gl.week9.bean.User;
import com.gl.week9.dao.AdminDao;

@Service
public class AdminService {
   @Autowired
   AdminDao adminDao;
	
   public List<Admin> getAllAdmin() {
		return adminDao.findAll();
	}
	public String storeAdminInfo(Admin admin) {
		
		if(adminDao.existsById(admin.getId())) {
					return "User id must be unique";
		}else {
					adminDao.save(admin);
					return "user stored successfully";
		}
}
	public String deleteAdminInfo(int id) {
		if(!adminDao.existsById(id)) {
			return "Books details not present";
			}else {
			adminDao.deleteById(id);
			return "Books deleted successfully";
			}	
	}
	public String updateAdminInfo(Admin admin) {
		if(!adminDao.existsById(admin.getId())) {
			return "user details not present";
			}else {
			Admin p	= adminDao.getById(admin.getId());	// if books not present it will give exception 
			p.setName(admin.getName());						// existing product price change 
			adminDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "user updated successfully";
			}	
	}

	
	

}
