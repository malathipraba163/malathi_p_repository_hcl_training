package com.gl.week9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.week9.bean.Books;
import com.gl.week9.bean.User;
import com.gl.week9.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;
	
	public List<User> getAllUsers() {
		return userDao.findAll();
	}
	public String storeUserInfo(User user) {
		
		if(userDao.existsById(user.getId())) {
					return "User id must be unique";
		}else {
					userDao.save(user);
					return "user stored successfully";
		}
}
	public String deleteUserInfo(int id) {
		if(!userDao.existsById(id)) {
			return "Books details not present";
			}else {
			userDao.deleteById(id);
			return "Books deleted successfully";
			}	
	}
	public String updateUserInfo(User user) {
		if(!userDao.existsById(user.getId())) {
			return "user details not present";
			}else {
			User p	= userDao.getById(user.getId());	// if books not present it will give exception 
			p.setUsername(user.getUsername());						// existing product price change 
			userDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "user updated successfully";
			}	
	}
	
	

}
