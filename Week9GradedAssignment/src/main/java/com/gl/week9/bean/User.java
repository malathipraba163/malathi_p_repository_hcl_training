package com.gl.week9.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Setter					// all setter methods. 
@Getter					// all getter methods 
@ToString				// toString() method 
@NoArgsConstructor			// empty constructor 
@RequiredArgsConstructor
public class User {
	@Id
	private int id;
    private String username;
    private String password;
	public String getUsername() {
		return username;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
    
}
