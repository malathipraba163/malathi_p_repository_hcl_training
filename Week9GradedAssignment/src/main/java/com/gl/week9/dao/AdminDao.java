package com.gl.week9.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.week9.bean.Admin;

public interface AdminDao extends JpaRepository<Admin, Integer>{
	Admin findByNameAndPassword(String name, String password);

}
