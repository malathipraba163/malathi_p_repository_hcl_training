package com.gl.week9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gl.week9.bean.Books;

@Repository
public interface BooksDao extends JpaRepository<Books, Integer> {
	

}
