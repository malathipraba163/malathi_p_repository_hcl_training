package com.gl.week9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.gl.week9.bean.LikeBooks;

public interface LikeBooksDao extends JpaRepository<LikeBooks, Integer> {

}
