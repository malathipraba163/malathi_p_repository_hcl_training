package com.gl.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.gl.miniproject.bean.Admin;
import com.gl.miniproject.bean.Menu;
import com.gl.miniproject.bean.User;
import com.gl.miniproject.service.MenuService;

@Controller
@RestController("/menu")
public class MenuController {
	@Autowired
	MenuService menuService;


	@GetMapping("/addMenu")
	public ModelAndView addMenu(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("admin") Admin admin) {
		
		ModelAndView mav = new ModelAndView("addMenu");
		mav.addObject("name", admin.getName());
		mav.addObject("menu", new Menu());
		
		return mav;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addMenu(@ModelAttribute("menu") Menu menu, Model m) {

		menuService.addMenu(menu);

		

		String menuName = menu.getItem();
		m.addAttribute("menuName", menuName);

		List<Menu> list = menuService.viewmenu();
		m.addAttribute("list", list);

		return "addMenu";
	}

	@RequestMapping(value = "/deleteFromMenu", method = RequestMethod.GET)
	public ModelAndView deleteMenu(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("menu") Menu menu) {

		System.out.println("calling delete method...");
		ModelAndView mav = null;

		menuService.deleteMenu(menu);

		mav = new ModelAndView("adminPage");
		List<Menu> list = menuService.viewmenu();
		mav.addObject("list", list);
		
		mav.addObject("menuName", menu.getItem());

		return mav;
	}

	@RequestMapping(value = "/adminPage", method = RequestMethod.GET)
	public ModelAndView showAdminPage(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("admin") Admin admin) {

		ModelAndView mav = null;

		mav = new ModelAndView("adminPage");
		List<Menu> list = menuService.viewmenu();
		mav.addObject("list", list);
		mav.addObject("name", admin.getName());

		return mav;
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public ModelAndView showWelcomePage(HttpServletRequest request, HttpServletResponse response,
			@ModelAttribute("user") User user) {

		ModelAndView mav = null;

		mav = new ModelAndView("welcome");
		List<Menu> list = menuService.viewmenu();
		mav.addObject("list", list);
		mav.addObject("name", user.getName());

		return mav;
	}

}
