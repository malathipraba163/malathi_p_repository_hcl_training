package com.gl.miniproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gl.miniproject.bean.User;
import com.gl.miniproject.service.UserService;

@Controller
public class RegisterController {
	@Autowired
	UserService service;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("user") User user) {
		
		service.save(user);
		
		return "login";
	}

}
