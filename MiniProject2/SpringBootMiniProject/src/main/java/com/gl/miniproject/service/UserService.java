package com.gl.miniproject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.miniproject.bean.User;
import com.gl.miniproject.dao.UserDao;


@Service
public class UserService {
	@Autowired
	UserDao userDao;

	

	public void save(User user) {
		userDao.save(user);
	}

	public User login(String name, String password) {
		User user = userDao.findByNameAndPassword(name, password);
		return user;
	}

	public List<User> showUser() {
		List<User> user = userDao.findAll();
		return user;
	}
	

}
