package com.gl.miniproject.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.miniproject.bean.Menu;
import com.gl.miniproject.dao.MenuDao;

@Service
public class MenuService {
	@Autowired
	MenuDao menuDao;
	public List<Menu> viewmenu() {
		List<Menu> menu = menuDao.findAll();
		return menu;
	}

	public void addMenu(Menu menu) {
		menuDao.save(menu);
		
	}

	public void deleteMenu(Menu menu) {
		
		Optional<Menu> list =menuDao.findById(menu.getItemid());
		
		if(list.isPresent()) {
			menuDao.delete(list.get());
		}
		else {
			throw new RuntimeException("menu not found");
		}
	}

}
