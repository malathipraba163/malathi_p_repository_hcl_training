package com.gl.miniproject.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger {
	@Bean
	public Docket showmenuApi() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).groupName("SurabiRestaurant").select()
				.apis(RequestHandlerSelectors.basePackage("com.gl.miniproject.controller")).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Welcome SurabiRestaurant ").description("SurabiRestaurant Good Food ,Good Life")
				.termsOfServiceUrl("http://fakesurabirestaurant.com")
				.contact((new Contact("SurabiRestaurant owner", "http://fakesurabirestaurant/Chennai.com", "surabihotel@gmail.com")))
				.license("SurabiRestaurant License").licenseUrl("Terms of Service").version("Apachea 2.0").build();

	}

}
