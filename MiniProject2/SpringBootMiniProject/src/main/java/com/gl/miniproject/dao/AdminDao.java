package com.gl.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.miniproject.bean.Admin;

public interface AdminDao extends JpaRepository<Admin, Integer> {
	Admin findByNameAndPassword(String name, String password);

}
