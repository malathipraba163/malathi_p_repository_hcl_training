package com.gl.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.miniproject.bean.Menu;

public interface MenuDao extends JpaRepository<Menu, Integer> {
	

}
