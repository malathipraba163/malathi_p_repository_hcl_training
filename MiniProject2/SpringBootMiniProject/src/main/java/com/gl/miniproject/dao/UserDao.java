package com.gl.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.miniproject.bean.User;

public interface UserDao  extends JpaRepository<User, Integer>{
	User findByNameAndPassword(String name, String password);

}
