package com.gl.miniproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.miniproject.bean.Admin;
import com.gl.miniproject.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao admindao;
	
	public void save(Admin admin) {
		admindao.save(admin);
	}	
	
	
	public Admin adminLogin(String name, String password) {
		Admin admin = admindao.findByNameAndPassword(name, password);
	  	return admin;
	  }
	

}
