package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com")		// Enable @RestController, @Service, @Repository 
@EntityScan(basePackages = "com.gl.miniproject.bean")							// Enable entity class 
@EnableJpaRepositories(basePackages = "com.gl.miniproject.dao")
public class SpringBootMiniProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMiniProjectApplication.class, args);
		System.out.println("Server port number 8080 is running ");
	}

}
