package com.gl.week11.assignment.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gl.week11.assignment.bean.Admin;
import com.gl.week11.assignment.dao.AdminDao;

@Service
public class AdminService {
	@Autowired
	AdminDao adminDao;
	
	public String signUp(Admin admin) {
		Admin adm=adminDao.findByUsernameAndPassword(admin.getUsername(), admin.getPassword());
		if(Objects.nonNull(adm)) {
			return "Username Already Exists,Try another one";
			}else {
			adminDao.save(admin);
			return "User Registered Successfully";
			}
		}
	public String signIn(Admin admin) {
		Admin ad=adminDao.findByUsernameAndPassword(admin.getUsername(), admin.getPassword());
		if(Objects.nonNull(ad)) {
			return "Login successfully!!";
		}else {
			return "Incorrect Password or Username";
		}
	}

}
