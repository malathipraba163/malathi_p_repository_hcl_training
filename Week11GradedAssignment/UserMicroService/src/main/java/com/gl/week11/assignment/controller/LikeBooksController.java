package com.gl.week11.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.week11.assignment.bean.LikeBooks;
import com.gl.week11.assignment.service.LikeBooksService;

@RestController
@RequestMapping("/likebooks")
public class LikeBooksController {
	@Autowired
	LikeBooksService likebooksService;
	
	@GetMapping(value = "getAllBooks",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LikeBooks> getAlllikebooksInfo() {
		return likebooksService.getAlllikebooks();
	}
	@PostMapping(value = "storeBooks",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storelikebooksInfo(@RequestBody LikeBooks book) {
		
				return likebooksService.storelikebooksInfo(book);
	}
	
	@DeleteMapping(value = "deleteBooks/{id}")
	public String storelikebooksInfo(@PathVariable("id") int id) {
					return likebooksService.deletelikebooksInfo(id);
	}
	
	@PatchMapping(value = "updateBooks")
	public String updatelikebooksInfo(@RequestBody LikeBooks book) {
					return likebooksService.updatelikebooksInfo(book);
	}
	
	


}
