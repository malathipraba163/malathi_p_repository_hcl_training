package com.gl.week11.assignment.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;


import com.gl.week11.assignment.bean.LikeBooks;
import com.gl.week11.assignment.dao.LikeBooksDao;
import com.gl.week11.assignment.dao.UserDao;
import com.gl.week11.assignment.service.LikeBooksService;
@SpringBootTest
class LikeBooksServiceTest {
	@InjectMocks
	LikeBooksService likebooksService;
	
	@Mock
	LikeBooksDao likebooksDao;
	@Mock
	UserDao userDao;

	@Test
	void testGetAlllikebooks() {
		//fail("Not yet implemented");
		List<LikeBooks> list = new ArrayList<LikeBooks>();
		LikeBooks book1 = new LikeBooks();
		LikeBooks book2 = new LikeBooks();
		LikeBooks book3 = new LikeBooks();
		list.add(book1);
		list.add(book2);
		list.add(book3);
		when(likebooksDao.findAll()).thenReturn(list);
		List<LikeBooks> bookList = likebooksService.getAlllikebooks();
		assertEquals(3, bookList.size());
		verify(likebooksDao, times(1)).findAll();
	}
	
    
	@Test
	void testStorelikebooksInfo() {
		//fail("Not yet implemented");
		LikeBooks book = new LikeBooks();
		likebooksService.storelikebooksInfo(book);
		verify(likebooksDao, times(1)).save(book);
	}
    @Disabled
	@Test
	void testDeletelikebooksInfo() {
		fail("Not yet implemented");
	}
    @Disabled
	@Test
	void testUpdatelikebooksInfo() {
		fail("Not yet implemented");
	}

}
