import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-adminregister',
  templateUrl: './adminregister.component.html',
  styleUrls: ['./adminregister.component.css']
})
export class AdminregisterComponent implements OnInit {
  storeMsg:string="";

  constructor(public pser:MyserviceService) { }

  ngOnInit(): void {
  }

  storeAdmin(registerRef:NgForm){
    this.pser.storeAdminDetails(registerRef.value).
    subscribe(res=>this.storeMsg=res,error=>console.log(error));  
  }

}
