import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Admin } from './admin';
import { Books } from './books';
import { Users } from './users';

@Injectable({
  providedIn: 'root'
})
export class MyserviceService {

  constructor(public http:HttpClient) { }
  storeAdminDetails(admin:Admin):Observable<string>{
    return this.http.post("http://localhost:8282/admin/signUp",admin,{responseType:"text"})

  }
  loadUserDetails():Observable<Users[]>{
     return this.http.get<Users[]>("http://localhost:8282/user/getAllUser")
  }
  storeUserDetails(user:Users):Observable<string>{
    return this.http.post("http://localhost:8282/user/storeUser",user,{responseType:"text"})
  }
  deleteUserDetails(id:number):Observable<string>{
    return this.http.delete("http://localhost:8282/user/deleteUser/"+id,{responseType:"text"})
  }
  updateUserInfo(user:any):Observable<string>{
    return this.http.patch("http://localhost:8282/user/updateUser",user,{responseType:"text"})
  }
  storeBooksDetails(books:Books):Observable<string>{
    return this.http.post("http://localhost:8282/admin/signUp",books,{responseType:"text"})

  }
  loadBooksDetails():Observable<Books[]>{
     return this.http.get<Books[]>("http://localhost:8282/books/getAllProduct")
  }
  storeBookDetails(books:Books):Observable<string>{
    return this.http.post("http://localhost:8282/books/storeProduct",books,{responseType:"text"})
  }
  deleteBooksDetails(id:number):Observable<string>{
    return this.http.delete("http://localhost:8282/books/deleteBooks/"+id,{responseType:"text"})
  }
  updateBooksInfo(books:any):Observable<string>{
    return this.http.patch("http://localhost:8282/books/updateBooks",books,{responseType:"text"})
  }
}
