import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Books } from '../books';
import { MyserviceService } from '../myservice.service';

@Component({
  selector: 'app-displaybooks',
  templateUrl: './displaybooks.component.html',
  styleUrls: ['./displaybooks.component.css']
})
export class DisplaybooksComponent implements OnInit {
   books:Array<Books>=[];
  constructor(public router:Router,public pser:MyserviceService) { }

  ngOnInit(): void {
  }
  loadBooks():void{
    this.pser.loadBooksDetails().subscribe(res=>this.books=res);
    
  }

}
