import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminregisterComponent } from './adminregister/adminregister.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DisplaybooksComponent } from './displaybooks/displaybooks.component';
import { LoginComponent } from './login/login.component';



const routes: Routes = [
  {path:"login",component:LoginComponent},
  {path:"home",component:DashboardComponent},
  {path:"adminregister",component:AdminregisterComponent},
  {path:"displaybooks",component:DisplaybooksComponent}

  
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
