import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Books } from './books';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(public http:HttpClient) { }

  loadProductDetails():Observable<Books[]> {
    return this.http.get<Books[]>("http://localhost:8282/books/getAllProduct")
  }

  storeProductDetails(books:Books):Observable<string>{
    return this.http.post("http://localhost:8282/books/storeProduct",books,{responseType:'text'})
  }  

  deleteProductDetails(id:number):Observable<string>{
    return this.http.delete("http://localhost:8282/books/deleteBooks/"+id,{responseType:'text'});
  }

  updateProductInfo(books:any):Observable<string>{
    return this.http.patch("http://localhost:8282/books/updateBooks",books,{responseType:'text'})
  }

}
