import { Pipe, PipeTransform } from "@angular/core";


@Pipe({name:"mypipe"})
export class MyPipe implements PipeTransform {
    // life cycle method 
    transform(value : any) {
        console.log(value);
        return value+" Rs ";
    }
}
