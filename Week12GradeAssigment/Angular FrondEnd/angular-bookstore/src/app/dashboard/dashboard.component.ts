import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Books } from '../books';
import { MyserviceService } from '../myservice.service';
import { Users } from '../users';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  adminName:string="";
  users:Array<Users>=[];
  books:Array<Books>=[];
  storeMsg:string="";
  storeBooksMsg:string="";
  deleteMsg:string="";
  deleteBooksMsg:string="";
  updateMsg:string="";
  updateBooksMsg:string="";
  flag:boolean=false;
  flag1:boolean=false;
  id:number=0;
  username:string="";
  name:string="";
  

  constructor(public router:Router,public pser:MyserviceService) { }

  ngOnInit(): void {
    let obj=sessionStorage.getItem("adminname");
    if(obj!=null){
      this.adminName=obj;

    }
    }
    logout(){
      sessionStorage.removeItem("adminname");
      this.router.navigate(["login"]);
    }
    loadUsers():void{
      this.pser.loadUserDetails().subscribe(res=>this.users=res);
    }
    storeUser(userRef:NgForm){
      this.pser.storeUserDetails(userRef.value).subscribe(res=>this.storeMsg=res,error=>console.log(error),
      ()=>this.loadUsers());
      }
      deleteUser(id:number){
        this.pser.deleteUserDetails(id).
        subscribe(res=>this.deleteMsg=res,error=>console.log(error),()=>this.loadUsers());

      }
      updateUser(user:Users){
        this.flag=true;
        this.id=user.id;
        this.username=user.username;
      }
      updateUserName(){
        let user={"id":this.id,"username":this.username}
        this.pser.updateUserInfo(user).subscribe(result=>this.updateMsg=result,
          error=>console.log(error),
          ()=>{
            this.loadUsers();
            this.flag=false;
          }
          )
      }
      loadBooks():void{
        this.pser.loadBooksDetails().subscribe(res=>this.books=res);
      }
      storeBooks(userRef:NgForm){
        this.pser.storeBookDetails(userRef.value).subscribe(res=>this.storeBooksMsg=res,error=>console.log(error),
        ()=>this.loadBooks());
        }
        deleteBooks(id:number){
          this.pser.deleteBooksDetails(id).
          subscribe(res=>this.deleteBooksMsg=res,error=>console.log(error),()=>this.loadBooks());
  
        }
        updateBooks(books:Books){
          this.flag=true;
          this.id=books.id;
          this.username=books.name;
        }
        updateBooksName(){
          let books={"id":this.id,"name":this.name}
          this.pser.updateBooksInfo(books).subscribe(result=>this.updateBooksMsg=result,
            error=>console.log(error),
            ()=>{
              this.loadUsers();
              this.flag=false;
            }
            )
        }



}
  

