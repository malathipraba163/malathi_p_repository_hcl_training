package com.gl.week11.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gl.week11.assignment.bean.Books;
import com.gl.week11.assignment.dao.BooksDao;

@Service
public class BooksService {
@Autowired
	
	BooksDao booksDao;
	
	public List<Books> getAllBooks() {
		return booksDao.findAll();
	}

}
