package com.gl.week11.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages = "com.gl.week11.assignment")
@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.gl.week11.assignment.dao")
@EntityScan(basePackages = "com.gl.week11.assignment.bean")
@EnableSwagger2
public class UserMicroServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserMicroServiceApplication.class, args);
		System.err.println("Admin service runing on port number 8383");
		
	}
	@Bean						// container will call this method when string boot load the application. 
	public Docket api() {
		System.out.println("object created..");
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.gl.week11.assignment.controller")).build();
	}

}
