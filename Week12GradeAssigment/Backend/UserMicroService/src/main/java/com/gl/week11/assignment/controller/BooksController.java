package com.gl.week11.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.week11.assignment.bean.Books;
import com.gl.week11.assignment.service.BooksService;

@RestController
@RequestMapping("/books")
public class BooksController {
	@Autowired
	BooksService booksService;
	
	@GetMapping(value = "getAllBooks",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Books> getAllProductInfo() {
		return booksService.getAllBooks();
	}

}
