package com.gl.week11.assignment.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gl.week11.assignment.bean.User;
import com.gl.week11.assignment.dao.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao userDao;
	
	public  String signUp(User user ) {
		User use=userDao.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		if(Objects.nonNull(use)) {
			return "Username Already Exists,Try another one";
			}else {
			userDao.save(user);
			return "User Registered Successfully";
			}
		}
	public String signIn(User user) {
		User use=userDao.findByUsernameAndPassword(user.getUsername(), user.getPassword());
		if(Objects.nonNull(use)) {
			return "Login successfully!!";
		}else {
			return "Incorrect Password or Username";
		}
	}

}
