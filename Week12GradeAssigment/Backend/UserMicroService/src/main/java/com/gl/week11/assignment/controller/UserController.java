package com.gl.week11.assignment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gl.week11.assignment.bean.User;
import com.gl.week11.assignment.service.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {
	@Autowired
	UserService userService;
	
	@PostMapping(value = "signUp",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String signUp(@RequestBody User user) {
		return userService.signUp(user);
	}
    @PostMapping(value="signIn",consumes=MediaType.APPLICATION_JSON_VALUE)
    public String signIn(@RequestBody User user) {
		return userService.signIn(user);
	}
    @GetMapping(value="logout")
    public String logout() {
    	return "Logout Successfully";
    }

}
