package com.gl.week11.assignment.test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;

import com.gl.week11.assignment.bean.Books;
import com.gl.week11.assignment.dao.BooksDao;
import com.gl.week11.assignment.dao.UserDao;
import com.gl.week11.assignment.service.BooksService;
@SpringBootTest
class BooksServiceTest {
	@InjectMocks
	BooksService booksService;
	
	@Mock
	BooksDao booksDao;
	
	@Mock
	UserDao userDao;

	@Test
	void testGetAllBooks() {
		//fail("Not yet implemented");
		List<Books> list = new ArrayList<Books>();
		Books book1 = new Books();
		Books book2 = new Books();
		Books book3 = new Books();
		list.add(book1);
		list.add(book2);
		list.add(book3);
		when(booksDao.findAll()).thenReturn(list);
		List<Books> bookList = booksService.getAllBooks();
		assertEquals(3, bookList.size());
		verify(booksDao, times(1)).findAll();
	}

	
	
	@Test
	void testStoreBooksInfo() {
		//fail("Not yet implemented");
		Books book = new Books();
		booksService.storeBooksInfo(book);
		verify(booksDao, times(1)).save(book);
	}

	@Disabled
	@Test
	void testDeleteBooksInfo() {
		//fail("Not yet implemented");
		Books book = new Books();
		when(booksDao.findById(book.getId())).thenReturn(Optional.of(book));
		assertEquals("Deleted Successfully", booksService.deleteBooksInfo(1));
		verify(booksDao, times(1)).delete(book);
		
//		// For failure
//			Exception e = assertThrows(ProjectException.class, () -> {
// 		service.deleteBookById(2);
//				});
//				
//				assertEquals("Book Id not found", e.getMessage());
	}

	
	@Test
	void testUpdateBooksInfo() {
		//fail("Not yet implemented");
//		Books p = new Books();
//		when(booksDao.getById(p.getId())).thenReturn(Optional.of(p));
//		booksService.updateBooksInfo(p);
//		verify(booksDao, times(1)).saveAndFlush(p);
	}

}
