package com.gl.week11.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gl.week11.assignment.bean.Admin;
@Repository
public interface AdminDao extends JpaRepository<Admin, String>{
	public Admin findByUsernameAndPassword(@Param("username") String username,@Param("password") String password);

}
