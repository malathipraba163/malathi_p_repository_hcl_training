package com.gl.week11.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gl.week11.assignment.bean.Books;
@Repository
public interface BooksDao extends JpaRepository<Books, Integer> {

}
