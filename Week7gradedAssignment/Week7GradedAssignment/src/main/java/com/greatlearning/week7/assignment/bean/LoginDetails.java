package com.greatlearning.week7.assignment.bean;

public class LoginDetails {
	private String name;
	private String password;
	
	public LoginDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	public LoginDetails(String name, String password) {
		super();
		this.name = name;
		this.password = password;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public  String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
