package com.greatlearning.week7.assignment.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.greatlearning.week7.assignment.bean.LoginDetails;
import com.greatlearning.week7.assignment.dao.LoginDetailsDao;

/**
 * Servlet implementation class LikedBookController
 */
@WebServlet("/LikedBookController")
public class LikedBookController extends HttpServlet {
	private static final long serialVersionUID = 1L;
private LoginDetailsDao ld;
    
    public void init() {
    	LoginDetails ld =new  LoginDetails();	
    }
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LikedBookController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String id = request.getParameter("likedbookid");
		LoginDetails ld = LoginController.currentUser;
		String name = ld.getName();
		String password = ld.getPassword();
		
		try {
		   LoginDetailsDao.addLiked(name, password, id);
		   RequestDispatcher rd = request.getRequestDispatcher("likedbook.jsp"); 
			HttpSession session=request.getSession();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><body><h1>Book added successfully to liked section</h1></body></html>");

		
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
