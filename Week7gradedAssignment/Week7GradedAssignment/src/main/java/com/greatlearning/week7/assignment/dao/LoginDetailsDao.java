package com.greatlearning.week7.assignment.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import com.greatlearning.week7.assignment.bean.LoginDetails;
import com.greatlearning.week7.assignment.resources.DBResources;


public class LoginDetailsDao {
	 public static boolean validate(LoginDetails ld) throws ClassNotFoundException {
	        boolean status = false;
	       
	        

	        try {
	        	Class.forName("com.mysql.jdbc.Driver");
	        	 Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/week7","root","Malathi28@");
	        	Connection con1= DBResources.getDbConnection();
	        
	            // Step 2:Create a statement using connection object
	        	
	            PreparedStatement preparedStatement=con1.prepareStatement("select * from logindetails where name=? and password=?");
	            preparedStatement.setString(1, ld.getName());
	            preparedStatement.setString(2, ld.getPassword());

	            System.out.println(preparedStatement);
	            ResultSet rs = preparedStatement.executeQuery();
	            status = rs.next();

	            } catch (SQLException e) {
	            System.out.println(e);
	        }
	        return status;
	    }

	    private static void printSQLException(SQLException ex) {
	        for (Throwable e: ex) {
	            if (e instanceof SQLException) {
	                e.printStackTrace(System.err);
	                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
	                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
	                System.err.println("Message: " + e.getMessage());
	                Throwable t = ex.getCause();
	                while (t != null) {
	                    System.out.println("Cause: " + t);
	                    t = t.getCause();
	                }
	            }
	        }
	    }
	 public static int signup(LoginDetails ld) throws ClassNotFoundException {
	        int result=0;

	        Class.forName("com.mysql.jdbc.Driver");

	        try {
	        	Connection con=DBResources.getDbConnection();
	        
	            // Step 2:Create a statement using connection object
	           PreparedStatement pstmt=con.prepareStatement("insert into logindetails(?,?");
	           pstmt.setString(1, ld.getName());
	           pstmt.setString(2, ld.getPassword());
	           

	            System.out.println(pstmt);
	            result= pstmt.executeUpdate();
	           

	            } catch (SQLException e) {
	            System.out.println(e);
	        }
	        return result;
	    }
	 private Connection con;	
 	 private PreparedStatement pstmt;

	 public void addLater(String name, String password, String bookId) throws SQLException {
			pstmt = con.prepareStatement("insert into ReadLater(name,password,booksId) values(?,?,?)");
			PreparedStatement st2 = con.prepareStatement("select *from ReadLater");
			ResultSet rs = st2.executeQuery();
			while(rs.next()) {
			
				if(rs.getString(1).equals(name) && rs.getString(2).equals(password) && rs.getString(3).equals(bookId)) {
					return;	
				}
			}
			pstmt.setString(1, name);
			pstmt.setString(2, password);
			pstmt.setString(3, bookId);
			pstmt.executeUpdate();
			pstmt.close();
			st2.close();
			rs.close();
			
		}
	 private static Connection conn;	
 	 private static PreparedStatement pstm;
	 public static void addLiked(String name, String password, String bookId) throws SQLException {
			try {
				pstm = conn.prepareStatement("insert into likedbook(name,password,bookId) values(?,?,?)");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			PreparedStatement st2 = conn.prepareStatement("select *from likedbook");
			ResultSet rs = st2.executeQuery();
			while(rs.next()) {
			
				if(rs.getString(1).equals(name) && rs.getString(2).equals(password) && rs.getString(3).equals(bookId)) {
					return;	
				}
			}
			pstm.setString(1, name);
			pstm.setString(2, password);
			pstm.setString(3, bookId);
			pstm.executeUpdate();	
			pstm.close();
			st2.close();
			rs.close();
			
		}
		

	   

}
